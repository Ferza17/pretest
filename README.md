# pretest

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

#Run Project <br />
Pastikan komputer anda terinstall nodejs & npm.<br />
npm run dev untuk menjalankan program di mode development.<br />


#localhost:3000<br />
Halaman Login terdapat 2 Form yang pertama untuk nomor telepon<br />
dan yang kedua untuk password. Apabila user tidak memiliki account maka dapat melanjutkan ke halaman register.<br />

#http://localhost:3000/registration<br />
Halaman Register terdapat 3 Form yang pertama untuk nomor telepon, kedua password, ketiga negara. <br />
untuk geolocation diakses di navigator dengan user permission. Kendala dibagian ini yaitu tidak berhasil mendapatkan device token.<br />

#http://localhost:3000/verification<br />
Halaman verifikasi kode OTP. 1 Button "Verifikasi" untuk check kode OTP. apabila benar di redirect ke halaman /profile. <br />

#localhost:3000/profile<br />
Halaman profile untuk menampilkan data dari user. terdapat Navbar dan tombol logout untuk keluar dari aplikasi.<br />

#penjelasan framework <br />
Framework menggunakan nuxtjs untuk server side rendering. file components untuk component yang akan di gunakan di apps <br />
bagian form component menggunakan konsep reusable component. Bagian Pages untuk halaman dari web, nama file akan menjadi url di website. <br />
file store merupakan state management dari vue ( vuex ) untuk menyimpan data dari user. tujuanya agar pengiriman data antar component khususnya parent component <br  />
dan child component lebih mudah dan menghindari kompleksitas.
