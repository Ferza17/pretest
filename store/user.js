export const state = () => ({
  access_token: '',
  user_id: '',
  phone_number: 0,
  password: '',
  country: '',
  latlong: '',
  device_token: '',
  device_type: { ios: 0, android: 1, web: 2 },
  education: [],
  career: [],
  image: [],
})

export const mutations = {
  delete_profile(state, vuexContex) {
    //TODO  delete profile
  },
  upload_image(state, vuexContex) {
    // TODO upload image to APi
  },
}
